<?php

namespace App\Exceptions;

class UnAuthenticationException extends \Exception
{
    const PAYMENT_NOT_ACCEPTABLE = 10001;
    const TURNSTILE_LOCKED = 10002;
}