<?php

namespace App\Alarms;


use App\Contracts\Alarm;
use App\Enums\AlarmStatus;
use App\Exceptions\UnAuthenticationException;

class TurnstileAlarm implements Alarm
{
    /**
     * @var AlarmStatus
     */
    protected $status;

    public function __construct()
    {
        $this->status = AlarmStatus::OFF();
    }

    /**
     * {@inheritDoc}
     */
    public function fire($message, $code)
    {
        $this->status = AlarmStatus::ON();

        throw new UnAuthenticationException($message, $code);
    }

    /**
     * {@inheritDoc}
     */
    public function off()
    {
        // if already of don't do anything.
        if ($this->isOff()) {
            return;
        }

        $this->status = AlarmStatus::OFF();
    }

    /**
     * {@inheritDoc}
     */
    public function isOn()
    {
        return $this->status->is(AlarmStatus::ON);
    }

    /**
     * {@inheritDoc}
     */
    public function isOff()
    {
        return !$this->isOn();
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrentStatus(): string
    {
        return $this->status->getReadable();
    }
}