<?php

namespace App\Contracts;

interface Payment
{
    /**
     * Determine if payment acceptable.
     *
     * @return bool
     */
    public function isValid(): bool;
}