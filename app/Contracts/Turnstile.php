<?php

namespace App\Contracts;

use App\Enums\TurnstileStatus;

interface Turnstile
{
    /**
     * Get label of current status.
     *
     * @return string
     */
    public function getCurrentStatus(): string;

    /**
     * Get label of an alarm status.
     *
     * @return string
     */
    public function getAlarmStatus(): string;

    /**
     * Determine if payment is accepted to open turnstile.
     *
     * @param Payment $payment
     *
     * @throws \App\Exceptions\UnAuthenticationException
     * @return Turnstile
     */
    public function check(Payment $payment): self;

    /**
     * Changes status of turnstile.
     *
     * @param TurnstileStatus $status
     * @return Turnstile
     */
    public function changeStatus(TurnstileStatus $status): self;

    /**
     * Passing person through turnstile.
     *
     * @throws \App\Exceptions\UnAuthenticationException
     * @return Turnstile
     */
    public function passing(): self;

    /**
     * Determine if turnstile unlocked and ready to passing by persons.
     *
     * @return bool
     */
    public function isUnLocked(): bool;

    /**
     * Determine if turnstile locked to prevent persons from passing.
     *
     * @return bool
     */
    public function isLocked(): bool;
}