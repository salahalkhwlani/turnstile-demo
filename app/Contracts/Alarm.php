<?php

namespace App\Contracts;

interface Alarm
{
    /**
     * Trigger the alarm.
     *
     * @param string $message
     * @param int $code
     *
     * @return void
     * @throws \App\Exceptions\UnAuthenticationException
     */
    public function fire($message, $code);

    /**
     * Turn off the alarm.
     *
     * @return void
     */
    public function off();

    /**
     * Determine if the alarm on.
     *
     * @return bool
     */
    public function isOn();

    /**
     * Determine if the alarm off.
     *
     * @return bool
     */
    public function isOff();

    /**
     * Get label of current status.
     *
     * @return string
     */
    public function getCurrentStatus(): string;
}