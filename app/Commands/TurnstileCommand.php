<?php

namespace App\Commands;

use App\Exceptions\UnAuthenticationException;
use App\Payments\Coin;
use App\Turnstiles\SimpleTurnstile;
use LaravelZero\Framework\Commands\Command;
use PhpSchool\CliMenu\CliMenu;

class TurnstileCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'app:run';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Main menu for turnstile actions.';

    /**
     * @var SimpleTurnstile
     */
    private $turnstile;


    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->turnstile = app(SimpleTurnstile::class);

        $this->menu('What you want to do?')
            ->addItem('Check Turnstile Status', function (CliMenu $menu) {
                $this->notify('Current Status', $this->turnstile->getCurrentStatus());
            })
            ->addItem('Check An Alarm Status', function (CliMenu $menu) {
                $this->notify('Current Status', $this->turnstile->getAlarmStatus());
            })
            ->addItem('Insert a Coin', function (CliMenu $menu) {
                try {
                    $this->turnstile->check(new Coin(1));
                    $this->notify('Great', 'Your Payment acceptable');
                } catch (UnAuthenticationException $exception) {
                    $this->notify("Sorry", $exception->getMessage());
                }
            })
            ->addItem('Passing', function (CliMenu $menu) {
                try {
                    $this->turnstile->passing();
                    $this->notify("Great", 'Your Passed!');
                } catch (UnAuthenticationException $exception) {
                    $this->notify("Sorry", $exception->getMessage());
                }
            })
            ->setForegroundColour('green')
            ->setBackgroundColour('black')
            ->setWidth(1000)
            ->setPadding(10)
            ->setMargin(5)
            ->setExitButtonText("Abort")
            ->open();
    }
}
