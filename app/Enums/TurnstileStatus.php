<?php

namespace App\Enums;

use Elao\Enum\AutoDiscoveredValuesTrait;
use Elao\Enum\ReadableEnum;

/**
 * Class TurnstileStatus
 * @package App\Enums
 *
 * @method static TurnstileStatus UNLOCKED()
 * @method static TurnstileStatus LOCKED()
 */
final class TurnstileStatus extends ReadableEnum
{
    use AutoDiscoveredValuesTrait;

    const UNLOCKED = 1;
    const LOCKED = 0;

    public static function readables(): array
    {
        return [
            static::UNLOCKED => 'Unlocked',
            static::LOCKED => 'Locked',
        ];
    }
}