<?php

namespace App\Enums;

use Elao\Enum\AutoDiscoveredValuesTrait;
use Elao\Enum\ReadableEnum;

/**
 * Class AlarmStatus
 * @package App\Enums
 *
 * @method static AlarmStatus ON()
 * @method static AlarmStatus OFF()
 */
final class AlarmStatus extends ReadableEnum
{
    use AutoDiscoveredValuesTrait;

    const ON = 1;
    const OFF = 0;

    public static function readables(): array
    {
        return [
            static::OFF => 'Off',
            static::ON => 'On',
        ];
    }
}