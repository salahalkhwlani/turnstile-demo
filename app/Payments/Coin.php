<?php

namespace App\Payments;

use App\Contracts\Payment;

class Coin implements Payment
{
    /**
     * amount of coin.
     *
     * @var float
     */
    protected $amount = 0;

    /**
     * Coin constructor.
     *
     * @param float $amount
     */
    public function __construct(float $amount = 0.0)
    {
        $this->amount = $amount;
    }

    /**
     * Determine if payment acceptable.
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->amount > 0;
    }
}