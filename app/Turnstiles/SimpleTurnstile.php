<?php

namespace App\Turnstiles;

use App\Contracts\Payment;
use App\Contracts\Turnstile as TurnstileInterface;
use App\Enums\TurnstileStatus;
use App\Exceptions\UnAuthenticationException;

class SimpleTurnstile extends Turnstile
{
    /**
     * {@inheritDoc}
     */
    public function check(Payment $payment): TurnstileInterface
    {
        if (!$payment->isValid()) {
            $this->alarm->fire("Payment not acceptable or not enough.", UnAuthenticationException::PAYMENT_NOT_ACCEPTABLE);
        }

        if ($this->isUnLocked()) {
            return $this;
        }

        return $this->unlock();
    }

    /**
     * {@inheritDoc}
     */
    public function passing(): TurnstileInterface
    {
        if ($this->isLocked()) {
            $this->alarm->fire("You couldn't passing without payment.", UnAuthenticationException::TURNSTILE_LOCKED);
        }

        return $this->lock();
    }

    public function unlock(): TurnstileInterface
    {
        $this->alarm->off();

        return $this->changeStatus(TurnstileStatus::UNLOCKED());
    }

    public function lock(): TurnstileInterface
    {
        return $this->changeStatus(TurnstileStatus::LOCKED());
    }
}