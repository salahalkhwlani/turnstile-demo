<?php

namespace App\Turnstiles;

use App\Alarms\TurnstileAlarm;
use App\Contracts\Payment;
use App\Contracts\Turnstile as TurnstileInterface;
use App\Enums\TurnstileStatus;

abstract class Turnstile implements TurnstileInterface
{
    /**
     * @var TurnstileStatus
     */
    protected $status;

    /**
     * @var TurnstileAlarm
     */
    protected $alarm;

    public function __construct(TurnstileAlarm $alarm)
    {
        $this->status = TurnstileStatus::LOCKED();
        $this->alarm = $alarm;
    }

    /**
     * {@inheritDoc}
     */
    public function changeStatus(TurnstileStatus $status): TurnstileInterface
    {
        $this->status = $status;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrentStatus(): string
    {
        return $this->status->getReadable();
    }

    /**
     * {@inheritDoc}
     */
    public function getAlarmStatus(): string
    {
        return $this->alarm->getCurrentStatus();
    }

    /**
     * {@inheritDoc}
     */
    public function isUnLocked(): bool
    {
        return $this->status->is(TurnstileStatus::UNLOCKED);
    }

    /**
     * {@inheritDoc}
     */
    public function isLocked(): bool
    {
        return !$this->isUnLocked();
    }

    /**
     * {@inheritDoc}
     */
    abstract public function check(Payment $payment): TurnstileInterface;

    /**
     * {@inheritDoc}
     */
    abstract public function passing(): TurnstileInterface;
}