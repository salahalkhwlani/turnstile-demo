## Install

Just install packages using the composer.

```ssh
composer install
```

## Run The App

```ssh
# using builds version
./builds/application
# or using source code
php turnstile-cli
```

## Runt Test unit

```ssh
./vendor/bin/phpunit
```
