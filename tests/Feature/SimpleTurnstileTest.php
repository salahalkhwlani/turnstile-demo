<?php

namespace Tests\Feature;

use App\Payments\Coin;
use App\Turnstiles\SimpleTurnstile;
use Tests\TestCase;

class SimpleTurnstileTest extends TestCase
{
    /**
     * @var SimpleTurnstile
     */
    private $turnstile;

    /**
     * @expectedException \App\Exceptions\UnAuthenticationException
     * @expectedExceptionCode 10002
     */
    public function testCustomerPassesWithoutPayment(): void
    {
        $this->turnstile->passing();
    }

    public function testCustomerPassesWithOnePayment(): void
    {
        $this->turnstile->check(new Coin(1));

        $this->assertTrue($this->turnstile->isUnLocked(), 'fail unlock gate after customer paid');

        $this->turnstile->passing();

        $this->assertTrue($this->turnstile->isLocked(), 'fail lock gate after customer passes');
    }

    /**
     * @expectedException \App\Exceptions\UnAuthenticationException
     * @expectedExceptionCode 10002
     */
    public function testRaisingAnAlarmWhenGateLocked()
    {
        $this->turnstile->passing();

        $this->assertEquals('On', $this->turnstile->getAlarmStatus());
    }


    /**
     * @expectedException \App\Exceptions\UnAuthenticationException
     * @expectedExceptionCode 10001
     */
    public function testRaisingAnAlarmWhenCustomerTryInvalidPayment()
    {
        $this->turnstile->check(new Coin());

        $this->assertEquals('On', $this->turnstile->getAlarmStatus());
    }

    /**
     * @expectedException \App\Exceptions\UnAuthenticationException
     * @expectedExceptionCode 10002
     */
    public function testRaisingAnAlarmWhenGateLockedAndTurnOfAnAlarmAfterPayment()
    {
        $this->turnstile->passing();

        $this->assertEquals('On', $this->turnstile->getAlarmStatus());

        $this->turnstile->check(new Coin(1));

        $this->assertEquals('Off', $this->turnstile->getAlarmStatus());
    }

    public function testGracefullyEatingMoney()
    {
        $this->assertTrue($this->turnstile->isLocked());

        $this->turnstile->check(new Coin(1));
        $this->assertTrue($this->turnstile->isUnLocked(), 'fail unlock gate on first coin.');

        $this->turnstile->check(new Coin(1));
        $this->assertTrue($this->turnstile->isUnLocked(), 'fail unlock gate on second coin.');

        $this->turnstile->passing();
        $this->assertTrue($this->turnstile->isLocked(), 'fail lock gate after passing');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->turnstile = app(SimpleTurnstile::class);
    }
}
