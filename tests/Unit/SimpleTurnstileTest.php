<?php

namespace Tests\Unit;

use App\Payments\Coin;
use App\Turnstiles\SimpleTurnstile;
use Tests\TestCase;

class SimpleTurnstileTest extends TestCase
{
    /**
     * @var SimpleTurnstile
     */
    private $turnstile;

    public function testCanGetCurrentStatus(): void
    {
        $this->assertEquals('Locked',$this->turnstile->getCurrentStatus());
    }

    public function testCanGetAlarmStatus(): void
    {
        $this->assertEquals('Off',$this->turnstile->getAlarmStatus());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->turnstile = app(SimpleTurnstile::class);
    }
}
