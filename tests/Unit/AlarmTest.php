<?php

namespace Tests\Unit;

use App\Alarms\TurnstileAlarm;
use App\Exceptions\UnAuthenticationException;
use Tests\TestCase;

class AlarmTest extends TestCase
{
    /**
     * @var TurnstileAlarm
     */
    private $alarm;

    public function testAlarmOffByDefault()
    {
        $this->assertEquals('Off', $this->alarm->getCurrentStatus());
    }

    /**
     * @expectedException \App\Exceptions\UnAuthenticationException
     * @expectedExceptionCode 10
     */
    public function testCanGetLabelOfCurrentStatusWhenOn()
    {
        $this->alarm->fire('test', 10);
        $this->assertEquals('On', $this->alarm->getCurrentStatus());
    }

    /**
     * @expectedException \App\Exceptions\UnAuthenticationException
     * @expectedExceptionCode 10
     */
    public function testTurnOffAlarm()
    {
        $this->alarm->fire('test', 10);

        $this->assertTrue($this->alarm->isOn());
        $this->alarm->off();
        $this->assertTrue($this->alarm->isOff());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->alarm = app(TurnstileAlarm::class);
    }
}